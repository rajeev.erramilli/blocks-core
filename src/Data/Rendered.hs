{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Data.Rendered (render, value, mkRendered, Rendered) where

import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Binary  (Binary)
import           GHC.Generics (Generic)

data Rendered a = Rendered a String
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

render :: Rendered a -> String
render (Rendered _ s) = s

value :: Rendered a -> a
value (Rendered a _) = a

mkRendered :: (a -> String) -> a -> Rendered a
mkRendered renderer a = Rendered a (renderer a)
